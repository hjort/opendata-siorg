#!/bin/bash

# conversão para CSV (para o SQLite)
xml2 < Estrutura_Organizacional.xml | sed -n '/Dados_Cadastro/p' | sed 's/^\/Estrutura_Organizacional\/Orgao\/Dados_Cadastro\///' | awk 'BEGIN{FS="=";print"Codigo;Nome;Sigla;Codigo_Pai"}/^Codigo=/{if(c!=""){print c";"n";"s";"p};c=$2;n="";s="N/D";p=""}/^Nome=/{n=$2;gsub(/;/,",",n)}/^Sigla=/{s=$2;gsub(/;/,",",s)}/^Codigo_Pai=/{p=$2}' > orgaos.csv

# script para criação da estrutura e importação dos dados
cat > importar-orgaos.sql << EOF
DROP TABLE IF EXISTS orgaos;

CREATE TABLE orgaos (
  Codigo int primary key,
  Nome text,
  Sigla text,
  Codigo_Pai int
);

.separator ";"
.import orgaos.csv orgaos

CREATE INDEX orgaos_pai ON orgaos (Codigo_Pai);

.schema
.quit
EOF

# criação do banco em SQLite
rm -f orgaos.db
sqlite3 -batch -init importar-orgaos.sql orgaos.db

ls -la orgaos.db
