#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import sqlite3
import json

# busca orgao pelo id
def findbyid(id):

	cur = con.cursor()
	cur.execute('SELECT sigla, nome, codigo_pai FROM orgaos WHERE codigo = ?', (id, ))

	row = cur.fetchone()
	if row == None:
		return None

	o = {}
	o['id'] = id
	o['name'] = row['sigla']
	o['data'] = {}
	o['data']['nome'] = row['nome']
	#o['name'] = "%s - %s" % (row['sigla'], row['nome'])

	return o

# busca filhos pelo id do pai
def findchildren(id_pai):

	cur = con.cursor()
	cur.execute('SELECT codigo FROM orgaos WHERE codigo_pai = ?', (id_pai, ))

	filhos = []
	while True:

		row = cur.fetchone()
		if row == None:
			break

		orgao = findbyid(row['codigo'])
		filhos.append(orgao)

	return filhos

# codigo principal
def main():
	#root = 26 # PR
	root = 1929 # MF
	niveis = 2

	id = root

	global con
	con = sqlite3.connect('orgaos.db')
	con.row_factory = sqlite3.Row

	o = findbyid(id)
	filhos = findchildren(id)
	o['children'] = filhos

	for f in filhos:
		ff = findchildren(f['id'])
		if ff:
			f['children'] = ff

	jo = json.dumps(o)
	print jo

	con.close()

if __name__ == '__main__':
	main()

