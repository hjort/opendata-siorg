#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import sqlite3
import json

def main():
	id = 26

	con = sqlite3.connect('orgaos.db')
	con.row_factory = sqlite3.Row

	cur = con.cursor()
	cur.execute('SELECT * FROM orgaos WHERE codigo = ?', (id, ))

	row = cur.fetchone()

	#print "orgao: %s - %s" % (row[2], row[1])
	print "orgao: %s - %s" % (row['sigla'], row['nome'])

	o = {}
	o['sigla'] = row['sigla']
	o['nome'] = row['nome']

	jo = json.dumps(o)
	print jo

	con.close()

if __name__ == '__main__':
	main()

