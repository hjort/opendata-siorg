import sqlite3
import json

# busca orgao pelo id
def findbyid(id):

	cur = con.cursor()
	cur.execute('SELECT sigla, nome, codigo_pai FROM orgaos WHERE codigo = ?', (id, ))

	row = cur.fetchone()
	if row == None:
		return None

	o = {}
	o['id'] = id
	o['name'] = row['sigla']
	o['data'] = {}
	o['data']['nome'] = row['nome']
	#o['name'] = "%s - %s" % (row['sigla'], row['nome'])

	return o

# busca filhos pelo id do pai
def findchildren(id_pai):

	cur = con.cursor()
	cur.execute('SELECT codigo FROM orgaos WHERE codigo_pai = ?', (id_pai, ))

	filhos = []
	while True:

		row = cur.fetchone()
		if row == None:
			break

		orgao = findbyid(row['codigo'])
		filhos.append(orgao)

	return filhos

# codigo principal
def arvore(req, raiz=26):

	req.content_type = "application/json"
	#req.content_type = "text/plain"
	
	#return str(req)
	req.headers_out["Access-Control-Allow-Origin"] = "*";

	#root = 26 # PR
	#root = 1929 # MF
	#niveis = 2

	id = raiz

	global con
	con = sqlite3.connect('/var/www/siorg/orgaos.db')
	con.row_factory = sqlite3.Row

	o = findbyid(id)
	filhos = findchildren(id)
	if filhos:
		o['children'] = filhos

	if len(filhos) < 30:
		for f in filhos:
			ff = findchildren(f['id'])
			if ff:
				f['children'] = ff

	con.close()

	jo = json.dumps(o)
	return jo

