DROP TABLE IF EXISTS orgaos;

CREATE TABLE orgaos (
  Codigo int primary key,
  Nome text,
  Sigla text,
  Codigo_Pai int
);

.separator ";"
.import orgaos.csv orgaos

CREATE INDEX orgaos_pai ON orgaos (Codigo_Pai);

.schema
